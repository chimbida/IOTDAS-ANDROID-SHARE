package com.example.chimbida.meutestecompartilhariotdas;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.chimbida.meutestecompartilhariotdas.domain.Campo;
import com.example.chimbida.meutestecompartilhariotdas.domain.Canal;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getActionBar().setCustomView(R.layout.abs_layout);
        TextView title=(TextView)findViewById(getResources().getIdentifier("barra_titulo", "id", getPackageName()));
        title.setText(getString(R.string.app_name));

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent);
            }
        }
    }

    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            TextView texto = (TextView) findViewById(R.id.mostra_texto);

            texto.setText(sharedText);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.configuracoes, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_configuracoes:
                startActivity(new Intent(this,PreferenciasActivity.class));
                return true;
//            case R.id.sair:
//                //remover do bd
//                return true;
            default:
                return super.onMenuItemSelected(featureId,item);
        }
    }

    public void share2IOTDAS(View view) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        TextView texto = (TextView) findViewById(R.id.mostra_texto);

//        JSONObject jsonObject = new JSONObject();
//        jsonObject.accumulate("name", "yrdyr");
//        jsonObject.accumulate("country", person.getCountry());
//        jsonObject.accumulate("twitter", person.getTwitter());

        // 4. convert JSONObject to JSON to String
//        json = jsonObject.toString();

        Campo campo = new Campo();
        List<Campo> campos = new ArrayList<Campo>();
        Canal canal = new Canal();

        canal.setGuid(prefs.getString("guid_canal", "guid_canal"));
        campo.setGuid(prefs.getString("guid_campo", "guid_campo"));
        campo.setDados(texto.getText().toString());
        campos.add(campo);
        canal.setCampos(campos);

        post(JsonUtil.toJSon(canal));

    }



    public String post(final JSONObject data) {
        try {

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

            final URL url = new URL("https://requestb.in/1hknntt1");
            //final URL url = new URL(prefs.getString("uri_post", "https://requestb.in/1fs4l5j1" ));
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-type", "application/json");

            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            final OutputStream outputStream = connection.getOutputStream();
            final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

            writer.write(data.toString());
            writer.flush();
            writer.close();
            outputStream.close();

            connection.connect();

            final InputStream stream = connection.getInputStream();
            return new Scanner(stream, "UTF-8").next();
        } catch (Exception e) {
            Log.e("Your tag", "Error", e);
        }

        return null;
    }
}
