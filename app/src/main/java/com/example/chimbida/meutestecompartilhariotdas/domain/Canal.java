package com.example.chimbida.meutestecompartilhariotdas.domain;

import java.util.List;

/**
 * Created by chimbida on 20/06/2017.
 */

public class Canal {

    private String guid;

    private List<Campo> campos;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public List<Campo> getCampos() {
        return campos;
    }

    public void setCampos(List<Campo> campos) {
        this.campos = campos;
    }

    @Override
    public String toString() {
        return "Canal{" +
                "guid='" + guid + '\'' +
                ", campos=" + campos +
                '}';
    }
}
