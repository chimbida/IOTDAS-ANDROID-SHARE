package com.example.chimbida.meutestecompartilhariotdas.domain;

/**
 * Created by chimbida on 20/06/2017.
 */

public class Campo {

    private String guid;

    private String dados;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getDados() {
        return dados;
    }

    public void setDados(String dados) {
        this.dados = dados;
    }

    @Override
    public String toString() {
        return "Campo{" +
                "guid='" + guid + '\'' +
                ", dados='" + dados + '\'' +
                '}';
    }
}
