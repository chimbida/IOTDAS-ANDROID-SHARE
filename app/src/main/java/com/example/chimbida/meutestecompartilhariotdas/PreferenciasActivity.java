package com.example.chimbida.meutestecompartilhariotdas;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class PreferenciasActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_preferencias);
        addPreferencesFromResource(R.xml.preferencias);
    }
}
