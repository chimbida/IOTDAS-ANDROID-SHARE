package com.example.chimbida.meutestecompartilhariotdas;

import com.example.chimbida.meutestecompartilhariotdas.domain.Campo;
import com.example.chimbida.meutestecompartilhariotdas.domain.Canal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by chimbida on 20/06/2017.
 */

public class JsonUtil {

    public static JSONObject toJSon(Canal canal) {
        try {
            // Converter Objeto para JSON
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("guid", canal.getGuid());

//            JSONObject jsonAdd = new JSONObject();
//            jsonAdd.put("campo", canal.getCampos());
//
//            jsonObj.put("campos", jsonAdd);

            JSONArray jsonArr = new JSONArray();

            for (Campo cp : canal.getCampos()) {
                JSONObject pnObj = new JSONObject();
                pnObj.put("guid", cp.getGuid());
                pnObj.put("dados", cp.getDados());
                jsonArr.put(pnObj);
            }

            jsonObj.put("campos", jsonArr);

            return jsonObj;

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return null;

    }
}
